// dwjz: "1.1753"
// fundcode: "160222"
// gsz: "1.1820"  净值估算
// gszzl: "0.57"
// gztime: "2020-11-27 15:00"
// jzrq: "2020-11-26"
// name: "国泰国证食品饮料行业指数分级"
// 这里可以直接获取基金的净值
// $.ajax({
//     url: "https://fundgz.1234567.com.cn/js/160221.js",
//     dataType: "jsonp",
//     jsonpCallback: "jsonpgz",
//     crossDomain: true,
//     success: function (data) {
//         // console.log(data);
//         current_fund = data;
//         show(data);
//     }
// });
// 160224.js?callback=jQuery35107451170779030607_1607247678231&_=1607247678232:1 Uncaught ReferenceError: jsonpgz is not defined
function jsonpgz(data) {
    chrome.storage.local.get([data['fundcode']], function (items) {
        console.log("查看数据",data, items);
        if (items.hasOwnProperty([data['fundcode']])) {
            console.log("已经存在了");
            data['quantity'] = items[data['fundcode']];

            let tmplist = app.foundList.filter(function (item) {
                return item.fundcode == data.fundcode;
            });
            if (tmplist.length) {
                tmplist[0].dwjz = data.dwjz;
                tmplist[0].gsz = data.gsz;
                tmplist[0].gszzl = data.gszzl;
                tmplist[0].gztime = data.gztime;
                tmplist[0].jzrq = data.jzrq;
                tmplist[0].name = data.name;
            } else {
                app.foundList.push(data);
            }
            app.foundList = app.foundList.sort(function(a, b) {
                console.log(typeof(a.fundcode));
                result = Number.parseInt(a.fundcode) - Number.parseInt(b.fundcode);
                return result;
            });
        } else {
            // 查询到的数据
            chrome.storage.local.set({[data['fundcode']]:0}, function () {
                data['quantity'] = 0;
                app.foundList.splice(0, 0, data);
                app.foundList = app.foundList.sort(function(a, b) {
                    console.log(typeof(a.fundcode));
                    result = Number.parseInt(a.fundcode) - Number.parseInt(b.fundcode);
                    return result;
                });
            });
        }
    });

}


function getFundData(fundcode) {
    console.log('funddata', fundcode);
    if (!fundcode) {
        return
    }
    let ele = document.createElement( 'script' );
    ele.type = "text/javascript" ;
    ele.src = "https://fundgz.1234567.com.cn/js/" + fundcode + ".js?rt=" + new Date().getTime();
    ele.onerror = function (e) {
        console.log("not found");
    };
    ele.onload = function (e) {
        // console.log(e);
        document.body.removeChild(ele);
    };
    document.body.appendChild(ele);
}

function findFundData(event) {
    console.log("开始查找基金")
    event.preventDefault();
    getFundData(app.fundcode);
    return false
}

function removeData(index) {
    chrome.storage.local.remove(app.foundList[index]['fundcode'], function () {
        app.foundList.splice(index, 1);
    });
}

function initApp() {
    chrome.storage.local.get(null, function (items) {
        Object.keys(items).forEach(function(value) {
            console.log(value);
            getFundData(value);
        })
    });
}



const app = new Vue({
    el: "#app",
    data: {
        fundcode: '',
        foundList: []
    },
    computed: {

    },
    methods: {
        initApp,
        getFundData,
        removeData,
        findFundData
    },
    components: {
        funditem: {
            template: "#funditem",
            props: ["fund"],
            computed: {
                profit() {
                    return this.quantity * this.jzProfit;
                },
                jzProfit() {
                    return this.gsz - this.dwjz;
                }
            },
            data() {
                console.log(this);
                return this.fund;
            },
            watch: {
                quantity: function () {
                    console.log(this);
                    chrome.storage.local.set({[this.fundcode]:this.quantity}, function () {

                    });
                }
            },
            filters: {
                showPrice(price) {
                    return price.toFixed(4);
                }
            }
        }
    }
});

this.initApp();
